#include <vector>
#include "Trit.h"

using namespace std;

class TritSet {
private:
    vector<uint32_t> set;
    size_t sizeT; //количество тритов
    size_t firstSize; //первоночальный размер в тритах

    Trit getTrit(size_t index) const;
    void setTrit(size_t index, trit value);

    size_t getSize() const;

    size_t getLast() const;
    void getNewPlace(size_t index);
public:
    class TritProxy {
    private:
        TritSet &TrSet;
        size_t index;
        friend class TritSet;
    public:
        TritProxy( TritSet &tritSet,const size_t index);
        explicit operator Trit() const;

        TritProxy& operator =(trit rTrit);
        TritProxy& operator =(Trit rTrit);
        TritProxy& operator =(TritProxy proxy);

        bool operator ==(const Trit& rTrit) const;

        const Trit operator &(const Trit& rTrit) const;
        const Trit operator &(const TritProxy& proxy) const;
        friend const Trit operator &(const Trit& lTrit, const TritProxy& proxy);

        const Trit operator |(const Trit& hsr) const;
        const Trit operator |(const TritProxy& proxy) const;
        friend const Trit operator |(const Trit& lTrit, const TritProxy& proxy);

        const Trit operator !();

        friend ostream& operator <<(ostream& stream, TritProxy proxy);
    };

    explicit TritSet(size_t trit_number);

    TritSet(const TritSet& other);

    size_t capacity() const;

    Trit operator[] (size_t index) const;
    TritProxy operator[] ( size_t index);


    TritSet& operator =(TritSet rTrit);
    const TritSet operator &(TritSet rTrit);
    const TritSet operator |(TritSet rTrit);
    const TritSet operator !();
    TritSet& operator &=(TritSet rTrit);
    TritSet& operator |=(TritSet rTrit);
    TritSet& operator !=(TritSet rTrit);

    void shrink();

    void trim(size_t lastIndex);

    size_t length() const;

    size_t cardinality(Trit value);

};


