#include <iostream>
#include "TritSet.h"
#include <gtest/gtest.h>
int main (){
    Trit A(True);
    TritSet B(5);
    TritSet::TritProxy T(B,3);
    T=False;
    Trit D=A & T;
    cout << D <<"\n";
    ::testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}


TEST(TritSet,capacity) {
    TritSet set(3);
    size_t allocLenght = set.capacity();
    ASSERT_TRUE(allocLenght == 3);

    set[500] = True;
    allocLenght = set.capacity();
    ASSERT_TRUE(allocLenght == (500));

    set[500] = Unknown;
    set.shrink();
    size_t allocLenghtNew = set.capacity();
    ASSERT_TRUE(3 == allocLenghtNew);
}

TEST(TritSet, shrink){
    TritSet set(100);
    size_t allocLenght = set.capacity();
    set[50] = True;
    ASSERT_TRUE(allocLenght == set.capacity());
    set.shrink();
    ASSERT_TRUE(allocLenght == set.capacity());
    set[150] = True;
    allocLenght = set.capacity();
    set[150] = Unknown;
    set.shrink();
    ASSERT_TRUE(allocLenght > set.capacity());
    ASSERT_TRUE(set.capacity() == (100));
}

TEST(TritSet, length) {
    TritSet set(100);
    set[60] = True;
    ASSERT_TRUE(set.length() == 61);
    set[60] = Unknown;
    ASSERT_TRUE(set.length() == 1);
}


TEST(TritSet, trim){
    TritSet set(100);
    set[40] = set[41] = True;
    set.trim(41);
    ASSERT_TRUE(set[40] == True);
    ASSERT_TRUE(set[41] == Unknown);
}

TEST(TritSet, cardinality) {
    TritSet set(100);
    set[1] = set[2] = True;
    set[3] = set[4] = set[6] = set[9] = False;
    ASSERT_TRUE(set.cardinality(True) == 2);
    ASSERT_TRUE(set.cardinality(False) == 4);
    ASSERT_TRUE(set.cardinality(Unknown)== 4);
}


TEST(TritSet, operators){
    TritSet set(100);
    set[0] = False;
    set[1] = Unknown;
    set[2] = True;
    ASSERT_TRUE((set[0]&set[0]) == False); // &
    ASSERT_TRUE((set[0]&set[1]) == False);
    ASSERT_TRUE((set[0]&set[2]) == False);
    ASSERT_TRUE((set[1]&set[0]) == False);
    ASSERT_TRUE((set[2]&set[0]) == False);
    ASSERT_TRUE((set[1]&set[2]) == Unknown);
    ASSERT_TRUE((set[2]&set[1]) == Unknown);
    ASSERT_TRUE((set[1]&set[1]) == Unknown);
    ASSERT_TRUE((set[2]&set[2]) == True);


    ASSERT_TRUE((set[0]|set[0]) == False); // |
    ASSERT_TRUE((set[0]|set[1]) == Unknown);
    ASSERT_TRUE((set[1]|set[0]) == Unknown);
    ASSERT_TRUE((set[1]|set[1]) == Unknown);
    ASSERT_TRUE((set[1]|set[2]) == True);
    ASSERT_TRUE((set[2]|set[1]) == True);
    ASSERT_TRUE((set[2]|set[2]) == True);
    ASSERT_TRUE((set[2]|set[0]) == True);
    ASSERT_TRUE((set[0]|set[2]) == True);

    TritSet setA(100);  // &=
    setA[2] = False;
    setA[3] = True;
    setA[4] = Unknown;
    TritSet setB(200);
    setB[2] = True;
    setB[3] = True;
    setB[4] = True;
    setB[152] = True;
    setB[153] = False;
    setB[154] = Unknown;
    setA &= setB;
    ASSERT_TRUE(setA.capacity() == setB.capacity());
    ASSERT_TRUE(setA[2] == False);
    ASSERT_TRUE(setA[3] == True);
    ASSERT_TRUE(setA[4] == Unknown);
    ASSERT_TRUE(setA[152] == Unknown);
    ASSERT_TRUE(setA[153] == False);
    ASSERT_TRUE(setA[154] == Unknown);

    ASSERT_FALSE(setA[300] == True);

    TritSet setA1(100);  // |=
    setA1[2] = False;
    setA1[3] = True;
    setA1[4] = Unknown;
    TritSet setB1(200);
    setB1[2] = True;
    setB1[3] = True;
    setB1[4] = True;
    setB1[152] = True;
    setB1[153] = False;
    setB1[154] = Unknown;
    setA1 |= setB1;
    ASSERT_TRUE(setA1.capacity() == setB1.capacity());
    ASSERT_TRUE(setA1[2] == True);
    ASSERT_TRUE(setA1[3] == True);
    ASSERT_TRUE(setA1[4] == True);
    ASSERT_TRUE(setA1[152] == True);
    ASSERT_TRUE(setA1[153] == Unknown);
    ASSERT_TRUE(setA1[154] == Unknown);

    TritSet setC(100);  // !
    setC[0] = True;
    setC[1] = Unknown;
    setC[2] = False;
    TritSet setNoSetC = !setC;
    ASSERT_TRUE(setNoSetC[0] == False);
    ASSERT_TRUE(setNoSetC[1] == Unknown);
    ASSERT_TRUE(setNoSetC[2] == True);

}
